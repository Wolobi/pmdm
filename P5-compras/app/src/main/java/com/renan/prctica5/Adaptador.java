package com.renan.prctica5;

import android.app.Activity;

import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import android.widget.TextView;

import java.util.ArrayList;


public class Adaptador extends ArrayAdapter<Productos> {

    Activity contexto;

    public Adaptador(Activity context, ArrayList<Productos> listItems) {
        super(context, R.layout.lista_compra, listItems);
        this.contexto = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        ViewHolder holder;
        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.lista_compra, null);
            holder = new ViewHolder();
            holder.titulo = item.findViewById(R.id.textView_listita);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        holder.titulo.setText(getItem(position).getNombre());

        if (getItem(position).isComprado()){
            holder.titulo.setTextColor(Color.parseColor("#00FF00"));
            holder.titulo.setPaintFlags(holder.titulo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            holder.titulo.setTextColor(Color.parseColor("#FF0000"));
            holder.titulo.setPaintFlags(holder.titulo.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        holder.titulo.setText(getItem(position).getNombre());


        return item;
    }

    class ViewHolder {
        TextView titulo;
    }
}
