package com.renan.prctica5;

public class Productos {


    private String nombre;
    private boolean comprado;

    public String getNombre() {
        return nombre;
    }

    public Productos(String nombre) {
        this.nombre = nombre;
        this.setComprado(false);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isComprado() {
        return comprado;
    }

    public void setComprado(boolean comprado) {
        this.comprado = comprado;
    }
}
