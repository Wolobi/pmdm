package com.renan.prctica5;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import android.widget.Toast;


import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements EditDialog.Comunicador {

    private ListView listViewCompra;
    ArrayList<Productos> arrayList;
    Adaptador adaptador;
    Productos item;

    Boolean modificador;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();


        if (v.getId() == R.id.listView_listaCompra) {
            AdapterView.AdapterContextMenuInfo info =
                    (AdapterView.AdapterContextMenuInfo) menuInfo;

            item = (Productos) listViewCompra.getItemAtPosition(info.position);
            menu.setHeaderTitle(((Productos) listViewCompra.getItemAtPosition(info.position)).getNombre() + " listado");

            inflater.inflate(R.menu.menu_contextual, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {


            case R.id.EditTextOp:
                modificador = true;
                showDialog();
                adaptador.notifyDataSetChanged();
                return true;

            case R.id.ReiniciaTextOp:
                Productos producto = (Productos) listViewCompra.getItemAtPosition(info.position);
                arrayList.remove(info.position);
                Toast.makeText(this, producto.getNombre(), Toast.LENGTH_LONG).show();
                adaptador.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewCompra = (ListView) findViewById(R.id.listView_listaCompra);
        adaptador = new Adaptador(this, llenarArrayList());

        listViewCompra.setAdapter(adaptador);


        registerForContextMenu(listViewCompra);


        listViewCompra.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                item = (Productos) listViewCompra.getItemAtPosition(position);

                if (item.isComprado()) {
                    Toast.makeText(getApplicationContext(), "No comprado", Toast.LENGTH_LONG).show();
                    item.setComprado(false);

                } else {
                    Toast.makeText(getApplicationContext(), "Comprado", Toast.LENGTH_LONG).show();

                    item.setComprado(true);
                }

                adaptador.notifyDataSetChanged();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuPrincipal:
                modificador = false;
                showDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void showDialog() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        EditDialog editDialog = new EditDialog();
        editDialog.modificar = modificador;
        editDialog.show(fm, null);
    }

    private ArrayList<Productos> llenarArrayList() {
        arrayList = new ArrayList<>();
        arrayList.add(new Productos("Arroz"));
        arrayList.add(new Productos("Pan"));
        arrayList.add(new Productos("Leche"));
        arrayList.add(new Productos("Aceite"));
        arrayList.add(new Productos("Vinagre"));
        //Devolvemos el arrayList con sus datos completados
        return arrayList;
    }


    @Override
    public void onDialogMessage(String message) {
        if (message.length() < 2) {
            Toast.makeText(this, R.string.inv_nombre, Toast.LENGTH_LONG).show();
            return;
        }

        message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length());
        if (modificador == false) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getNombre().equals(message)) {
                    Toast.makeText(this, R.string.inv_nombre, Toast.LENGTH_LONG).show();
                    return;
                }

            }
            arrayList.add(new Productos(message));
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } else {

            Toast.makeText(this, R.string.inv_nombre, Toast.LENGTH_LONG).show();


            item.setNombre(message);

        }

        adaptador.notifyDataSetChanged();

    }
}
