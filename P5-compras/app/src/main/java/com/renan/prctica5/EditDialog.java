package com.renan.prctica5;



import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditDialog extends DialogFragment implements View.OnClickListener {
//https://www.youtube.com/watch?v=bkUHeXCX8XM
//https://www.youtube.com/watch?v=j1ySIiZz83I
//https://www.youtube.com/watch?v=yywHMG5k-OI&list=PLepSKPKAxOCMnf83Zv70HJnKUpt-JzwGu APRENDER ANDROID
    //https://www.youtube.com/watch?v=bOAsnpjJFQ8&list=PLepSKPKAxOCMnf83Zv70HJnKUpt-JzwGu&index=40 cilco de vida fragment
    TextView textView_alta;
    TextView textView_alta2;
    TextView textView_nombre;
    EditText editText_nombre;
    Button button_aceptar;
    Button button_cancelar;
    Boolean modificar;

    public void setModificar(Boolean modificar) {
        this.modificar = modificar;
    }

    Comunicador comunicator;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        comunicator=(Comunicador) activity;
    }

    interface Comunicador
    {
        public void onDialogMessage(String message);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_dialog,null);

        textView_alta = view.findViewById(R.id.textView_fragment_alta);


        textView_alta2 = view.findViewById(R.id.textView_fragment_alta2);



        if (modificar){
            textView_alta.setText("Modificar");
            textView_alta2.setText("Modificar el articulo");
        }else{
            textView_alta.setText(R.string.defrag_alta);
            textView_alta2.setText(R.string.defrag_alta2);
        }

        textView_nombre = view.findViewById(R.id.textView_fragment_nombre);
        textView_nombre.setText(R.string.defrag_nombre);


        editText_nombre = view.findViewById(R.id.editText_nombre);
        button_aceptar= view.findViewById(R.id.button_aceptar);
        button_cancelar= view.findViewById(R.id.button_cancelar);
        button_cancelar.setOnClickListener(this);
        button_aceptar.setOnClickListener(this);

        setCancelable(false);
        return view;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.button_aceptar){
            String texto = editText_nombre.getText().toString();
            comunicator.onDialogMessage(texto);
            dismiss();
        }else{
            dismiss();
        }
    }
}
