package com.renan.examen.utilidades;

public class Constantes {

    public static final String BD_NOTAS = "bdnotas.db";

    //Constantes campos tabla notas
    public static final String TABLA_NOTAS = "notas";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_CATEGORIAS = "categoria";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_ICONO = "icono";

    public static final int VERSION = 2;

    public static final String CONSULTAR_TABLA_NOTAS = "SELECT * FROM "+TABLA_NOTAS;
    public static final String ELIMINAR_NOTA = "DELETE FROM "+TABLA_NOTAS+" where "+CAMPO_ID+"=?";
    public static final String CREAR_TABLA_NOTAS = "CREATE TABLE " + TABLA_NOTAS + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CAMPO_CATEGORIAS + " TEXT NOT NULL, " + CAMPO_TITULO + " TEXT NOT NULL, " + CAMPO_DESCRIPCION + " TEXT NOT NULL, " + CAMPO_ICONO + " INTEGER NOT NULL)";
}
//db.delete("tablename","id=? and name=?",new String[]{"1","jack"});