package com.renan.controltrimestral.utilidades;

public class Constantes {

    public static final String BD_LIBROS = "bdlibros.db";

    //Constantes campos tabla notas
    public static final String TABLA_LIBROS = "LIBROS";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_TITULO = "TITULO";
    public static final String CAMPO_AUTOR = "AUTOR";
    public static final String CAMPO_PORTADA = "PORTADA";
    public static final String CAMPO_FAVORITO = "FAVORITO";

    public static final int VERSION = 1;

    public static final String CREAR_TABLA_NOTAS = "CREATE TABLE " + TABLA_LIBROS + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT)";
    public static final String CONSULTAR_TABLA_LIBROS = "SELECT * FROM "+TABLA_LIBROS;
    public static final String CONSULTAR_TABLA_LIBROS_AUTORES = "SELECT "+CAMPO_AUTOR+" FROM "+TABLA_LIBROS +" GROUP BY "+CAMPO_AUTOR;
    public static final String CONSULTAR_TABLA_LIBROS_AUTORES_AUTOR = "SELECT * FROM "+TABLA_LIBROS+" WHERE "+CAMPO_AUTOR+" = ";
 //   public static final String UPDATE_TABLA_LIBROS_FAVORITO = "UPDATE "+TABLA_LIBROS+"SET "+CAMPO_FAVORITO+" WHERE "+CAMPO_AUTOR+" = ";
}
//db.delete("tablename","id=? and name=?",new String[]{"1","jack"});UPDATE t1 SET col1 = col1 + 1;