package com.renan.myapplication;

public class Recursos {

    static String verificarNombre(String nombre) throws IllegalArgumentException {
        //Eliminamos los espacios delante, detrás y espacios duplicados en medio del String
        nombre = nombre.trim();
        nombre = nombre.toLowerCase();
        //Verificamos sí el nombre cumple con el patrón, de no ser asi lanzaremos la excepción
        if (nombre.matches("[a-záéíóúñ]{2,15}[.]?[\\s]?[a-záéíóúñ]{1,13}?")) {//Comprobando que la cadena sea valida
            //pasamos la cadena a minúscula

            //transformamos la primera letra de la cadena en mayúscula
            nombre = nombre.substring(0, 1).toUpperCase() + nombre.substring(1);
            // Verificamos si la cadena lleva un espacio en medio para tratarla dentro de la condicional
            if (nombre.contains(" ")) {
                //Separamos la cadena en dos a raiz de un regex " "
                String[] palabras = nombre.split(" ");
                //Tratamos la cadena para que ponga la primera letra de la segunda parte el nombre en Mayúscula
                nombre = palabras[0] + " " + palabras[1].substring(0, 1).toUpperCase() + palabras[1].substring(1);
            }
            //Return del nombre filtrado y válidado
            return nombre;
        }
        //Algo no ha ido bien, lanzamos excepción
        throw new IllegalArgumentException("Nombre inválido");//Lanza una excepcion al haber algo erroneo
    }

    static String verificarApellido(String apellido) throws IllegalArgumentException {
        //Eliminamos los espacios delante, detrás y espacios duplicados en medio del String
        apellido = apellido.trim();
        apellido = apellido.toLowerCase();
        //Verificamos sí el apellido cumple con el patrón, de no ser asi lanzaremos la excepción
        if (apellido.matches("[a-záéíóúñ\\s-.]{2,33}")) {//Comprobando que la cadena sea valida
            //pasamos la cadena a minúscula
            //transformamos la primera letra de la cadena en mayúscula
            apellido = apellido.substring(0, 1).toUpperCase() + apellido.substring(1);
            // Verificamos si la cadena lleva un espacio en medio para tratarla dentro de la condicional
            if (apellido.contains(" ")) {
                //Separamos la cadena en dos a raiz de un regex " "
                String[] palabras = apellido.split(" ");
                //Tratamos la cadena para que ponga la primera letra de la segunda parte el apellido en Mayúscula
                apellido = palabras[0] + " " + palabras[1].substring(0, 1).toUpperCase() + palabras[1].substring(1);
            }
            if (apellido.contains("-")) {
                //Separamos la cadena en dos a raiz de un regex " "
                String[] palabras = apellido.split("-");
                //Tratamos la cadena para que ponga la primera letra de la segunda parte el apellido en Mayúscula
                apellido = palabras[0] + "-" + palabras[1].substring(0, 1).toUpperCase() + palabras[1].substring(1);
            }
            //Return del apellido filtrado y válidado
            return apellido;
        }
        //Algo no ha ido bien, lanzamos excepción
        throw new IllegalArgumentException("Apellido inválido");//Lanza una excepcion al haber algo erroneo
    }
}
