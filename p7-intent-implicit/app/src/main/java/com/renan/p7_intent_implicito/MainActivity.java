package com.renan.p7_intent_implicito;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity  implements DialogoProgreso.Comunicador{

    Uri imageUri;
    private static int RESULT_LOAD_IMAGE = 1;
    Button buttonEnviar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonLoadImage = (Button) findViewById(R.id.button_selec_img);
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                abrirGaleria(arg0);
            }
        });

        buttonEnviar = findViewById(R.id.button_enviar);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    public void showDialog() {
        FragmentManager fm = getSupportFragmentManager();
        DialogoProgreso editDialog = new DialogoProgreso();
        editDialog.show(fm, null);
    }

        public void abrirGaleria(View v){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, RESULT_LOAD_IMAGE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            ImageView imageView = findViewById(R.id.imageView_avatar);
            imageUri = data.getData();
            imageView.setImageURI(imageUri);
            buttonEnviar.setEnabled(true);

    }}

    @Override
    public void onDialogMessage(boolean message) {
        if (!message){
            Toast.makeText(this, R.string.enviado, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, R.string.envio_cancelado, Toast.LENGTH_LONG).show();
        }
    }
}
