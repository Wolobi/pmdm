package com.renan.practica10.entidades;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;

import com.renan.practica10.R;

public class Nota {

    private int id;
    private String categoria;
    private String titulo;
    private String descripcion;
    private int icono;

    public final static int CATEGORIA_REUNION = 0;
    public final static int CATEGORIA_AVISO = 1;
    public final static int CATEGORIA_VARIOS = 2;

    public static String categoria(int num) {

        switch (num) {
            case CATEGORIA_REUNION:
                return "REUNIÓN";
            case CATEGORIA_AVISO:
                return "AVISO";
            default:
                return "VARIOS";
        }
    }

    public Nota(int id, String categoria, String titulo, String descripcion, int icono) {
        this.id = id;
        this.categoria = categoria;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.icono = icono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }
}
