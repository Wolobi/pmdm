package com.renan.practica10;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.renan.practica10.entidades.Nota;

import java.util.ArrayList;


public class Adaptador extends ArrayAdapter<Nota> {

    Activity contexto;
    private int layout;

    public Adaptador(Activity context,int layout, ArrayList<Nota> objects) {
        super(context, R.layout.vista_nota_list, objects);
        this.layout=layout;
        this.contexto = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolder holder;

        if (item == null) {

            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(layout, null);
            holder = new ViewHolder();
            holder.titulo = item.findViewById(R.id.textView_titulo);
            holder.categoria = item.findViewById(R.id.textView_categoria);
            holder.icono = item.findViewById(R.id.imageView_categoria);
            item.setTag(holder);

        } else {
            holder = (ViewHolder) item.getTag();

        }

        holder.titulo.setText(getItem(position).getTitulo());
        holder.categoria.setText(getItem(position).getCategoria());


        switch (getItem(position).getIcono()) {
            case 0:
                holder.icono.setImageResource(R.drawable.reunion);
                break;
            case 1:
                holder.icono.setImageResource(R.drawable.aviso);
                break;
            default:
                holder.icono.setImageResource(R.drawable.varios);
        }

        return item;
    }

    class ViewHolder {
        ImageView icono;
        //Peso del vehículo
        TextView titulo;
        //Potencia en caballos del vehículo
        TextView categoria;
    }
}
