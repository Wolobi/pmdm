package com.renan.practica10;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.renan.practica10.entidades.Nota;
import com.renan.practica10.utilidades.Constantes;

import java.util.ArrayList;

public class RegistrarNota extends AppCompatActivity {
    Spinner spinner;
    ConectorSQLiteHelper con;
    static boolean modificar = false;
    EditText editText_titulo;
    EditText editText_descripcion;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_nota);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        con = new ConectorSQLiteHelper(getApplicationContext(), Constantes.BD_NOTAS, null, Constantes.VERSION);

        //Captuuramos texto del campo descripcion
        editText_descripcion = findViewById(R.id.editText_registro_descripcion);
        editText_titulo = findViewById(R.id.editText_registro_titulo);


        //POBLAR SPINNER
        spinner = findViewById(R.id.spinner_categoria);
        ArrayList<String> alSpinner = new ArrayList<>();
        alSpinner.add(Nota.categoria(Nota.CATEGORIA_REUNION));
        alSpinner.add(Nota.categoria(Nota.CATEGORIA_AVISO));
        alSpinner.add(Nota.categoria(Nota.CATEGORIA_VARIOS));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, alSpinner);
        spinner.setAdapter(arrayAdapter);
        //FIN POBLAR SPINNER;

        Button boton_registrar = findViewById(R.id.button_aceptar);


        id = getIntent().getStringExtra("id");
        modificar = getIntent().getBooleanExtra("modificar", false);
        if (modificar) {
            int categoria = getIntent().getIntExtra("categoria", 1);
            spinner.setSelection(categoria);
            editText_titulo.setText(getIntent().getStringExtra("titulo"));
            editText_descripcion.setText(getIntent().getStringExtra("descripcion"));
        }

        boton_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guardarNota();

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    private void guardarNota() {
        //Capturamos texto del spinner
        String text_categoria = spinner.getSelectedItem().toString().trim();
        //Captuuramos texto del campo titulo

        String titulo = editText_titulo.getText().toString().trim();


        String descripcion = editText_descripcion.getText().toString().trim();

        if (titulo.equals("") || descripcion.equals("")) {
            if (titulo.equals("")) {
                Toast.makeText(getApplicationContext(), "Campo título vacio.", Toast.LENGTH_LONG).show();
                editText_titulo.setText("");
            }
            if (descripcion.equals("")) {
                Toast.makeText(getApplicationContext(), "Campo descripción vacio.", Toast.LENGTH_LONG).show();
                editText_descripcion.setText("");
            }
        } else {


            int r_icono = 0;
            switch (spinner.getSelectedItemPosition()) {
                case 0:
                    r_icono = Nota.CATEGORIA_REUNION;
                    break;
                case 1:
                    r_icono = Nota.CATEGORIA_AVISO;
                    break;
                default:
                    r_icono = Nota.CATEGORIA_VARIOS;
            }


            SQLiteDatabase db = con.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Constantes.CAMPO_CATEGORIAS, text_categoria);
            values.put(Constantes.CAMPO_TITULO, titulo);
            values.put(Constantes.CAMPO_DESCRIPCION, descripcion);
            values.put(Constantes.CAMPO_ICONO, r_icono);
//
//

            if (modificar) {
                db.update(Constantes.TABLA_NOTAS, values, Constantes.CAMPO_ID + "=" + id, null);
            } else {
                db.insert(Constantes.TABLA_NOTAS, Constantes.CAMPO_ID, values);
            }
            setResult(RESULT_OK);
            finish();
        }
    }

    private void guardarNotaEjemplo() {

        SQLiteDatabase db = con.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constantes.CAMPO_ID, 2);
        values.put(Constantes.CAMPO_CATEGORIAS, "hola");
        values.put(Constantes.CAMPO_TITULO, "rtaoo");
        values.put(Constantes.CAMPO_DESCRIPCION, "test");
        values.put(Constantes.CAMPO_ICONO, 99);

        Long idResultado = db.insert(Constantes.TABLA_NOTAS, Constantes.CAMPO_ID, values);
        Toast.makeText(getApplicationContext(), "Id Regsitro " + idResultado, Toast.LENGTH_LONG).show();

    }
}
