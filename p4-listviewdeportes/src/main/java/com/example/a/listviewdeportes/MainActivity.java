package com.example.a.listviewdeportes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private ListView listViewDeportes;//Declaración local de la variable que va a contener el listView de los deportes
    private Adaptador adaptador;//Declaración local del adaptador

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botonAceptar = findViewById(R.id.Button_aceptar);//Instanciando el botón de aceptar
        botonAceptar.setText(R.string.boton_aceptar_texto);//Asignando el texto al botón de aceptar

        TextView textView_info = findViewById(R.id.TextView_info);//Instanciando el TextView de la parte top de la aplicación
        textView_info.setText(R.string.info);//Asignando el texto al TextView de la parte top

        listViewDeportes = (ListView) findViewById(R.id.listviewBOTON); //Instanciando el listView
        adaptador = new Adaptador(this, llenarArrayList());//Instanciando el adaptador

        listViewDeportes.setAdapter(adaptador);//Asignando el adaptador al listView



        /*
         Método de escucha del listView
         */
        listViewDeportes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //Al hacer click se va a ejecutar
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Instanciando el deporte que corresponda a la posición tecleada
                Deportes item = (Deportes) listViewDeportes.getItemAtPosition(position);
                //Sí el checkbox esta marcado se le establecerá su posición contraria
                if (item.isCheckBoxDeporte()) {
                    item.setCheckBoxDeporte(false);
                } else {
                    item.setCheckBoxDeporte(true);
                }
                //Forzar la actualización del adaptador para que se pueda apreciar en pantalla
                adaptador.notifyDataSetChanged();
            }
        });

        /*
        Método de escucha del botón "Aceptar"
        */
        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LLamada al metodo que contiene el toast que dará la información al usuario
                montarToast();

            }
        });
    }

    /*
    Método que va a montar la cadena y entregar el mensaje al usuario
     */
    private void montarToast() {
        //Iniciamos contador para averiguar cuantas opciones hay marcadas
        int contador = 0;
        //Iniciamos la cadena que se entregará al usuario y le establecemos la cabecera del mensaje
        String mensaje = getResources().getString(R.string.te_gusta) + " ";
        //Bucle encargado de contar cuantas opciones hay marcadas
        for (int i = 0; i < adaptador.getCount(); i++) {
            if (adaptador.getItem(i).isCheckBoxDeporte()) {
                contador++;//Sí esta posición esta marcada añadele un uno en el contador
            }
        }
        //Sí el contador es mayor que 0 significa que hay algo marcado y por ende se ejecutará el código interno
        if (contador > 0) {
            //Volvemos a lanzar el mismo bucle, solo que ahora para montar la cadena de texto
            for (int i = 0; i < adaptador.getCount(); i++) {
                //sí esta posición del switch esta marcada
                if (adaptador.getItem(i).isCheckBoxDeporte()) {
                    //switch para montar el mensaje, según la posición del iten marcado
                    switch (i) {
                        case 0:
                            mensaje += getResources().getString(R.string.el_atletismo);
                            break;
                        case 1:
                            mensaje += getResources().getString(R.string.el_baloncesto);
                            break;
                        case 2:
                            mensaje += getResources().getString(R.string.el_futbol);
                            break;
                        case 3:
                            mensaje += getResources().getString(R.string.el_motociclismo);
                            break;
                        case 4:
                            mensaje += getResources().getString(R.string.la_natacion);
                            break;
                        case 5:
                            mensaje += getResources().getString(R.string.el_pingpong);
                            break;
                    }
                    //Bajamos el contador
                    contador--;
                    //Escribimos el penúltimo separador
                    if (contador == 1) {
                        mensaje += " y ";
                    }
                    //Escribimos las comas solo sí no están en las 2 últimas posiciones
                    if (contador != 0 && contador != 1) {
                        mensaje += ", ";
                    }
                }

            }
            //Lanzamos Toast
            Toast toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG);
            toast1.show();
        }
    }


    /*
    Método en el que vamos a asignar datos al arrayList
     */
    private ArrayList<Deportes> llenarArrayList() {
        ArrayList<Deportes> arrayList = new ArrayList<>();
        arrayList.add(new Deportes(R.drawable.atletismo, "Atletismo", false));
        arrayList.add(new Deportes(R.drawable.baloncesto, "Baloncesto", false));
        arrayList.add(new Deportes(R.drawable.futbol, "Fútbol", false));
        arrayList.add(new Deportes(R.drawable.motociclismo, "Motociclismo", false));
        arrayList.add(new Deportes(R.drawable.natacion, "Natación", false));
        arrayList.add(new Deportes(R.drawable.pingpong, "Ping Pong", false));

        //Devolvemos el arrayList con sus datos completados
        return arrayList;
    }

}
