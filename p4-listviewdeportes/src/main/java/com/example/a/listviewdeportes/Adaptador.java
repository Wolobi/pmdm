package com.example.a.listviewdeportes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<Deportes> {
    private Activity context;

    public Adaptador(Activity context, ArrayList<Deportes> listItems) {
        super(context,R.layout.lista_deportes,listItems);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolder holder;
        if(item==null) {
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.lista_deportes, null);

            holder = new ViewHolder();
            holder.imgFotoHolder = (ImageView) item.findViewById(R.id.imageView_foto);
            holder.titulo = (TextView)item.findViewById(R.id.textView_titulo);
            holder.checkBox = (CheckBox) item.findViewById(R.id.checkBox_deportes);
            item.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)item.getTag();
        }
        Deportes deporte = (Deportes) getItem(position);

        holder.imgFotoHolder.setImageResource(deporte.getImgFoto());
        holder.titulo.setText(deporte.getTitulo());
        holder.checkBox.setChecked(deporte.isCheckBoxDeporte());

        return item;
    }
    class ViewHolder {
        ImageView imgFotoHolder;
        TextView titulo;
        CheckBox checkBox;
    }
}
