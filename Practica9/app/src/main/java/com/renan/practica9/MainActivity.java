package com.renan.practica9;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Vehiculo> arrayList;
    ListView listaVehiculos;
    Adaptador adaptador;
    Activity activity = this;
    static int elegirVehiculo = 0;
    Vehiculo v;
    Spinner spin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_icono_coche_round);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        Toolbar actionBarToolbar = findViewById(R.id.action_bar);
        actionBarToolbar.setTitleTextColor(getResources().getColor(R.color.titulo));

        spin = findViewById(R.id.postfield_category);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adaptador.clear();
                switch (position) {
                    case 0:
                        adaptador = new Adaptador(activity, llenarArrayList(0));
                        elegirVehiculo = 0;
                        listaVehiculos.setAdapter(adaptador);
                        break;
                    case 1:
                        adaptador = new Adaptador(activity, llenarArrayList(1));
                        elegirVehiculo = 1;
                        listaVehiculos.setAdapter(adaptador);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listaVehiculos = findViewById(R.id.ListView_vehiculos);
        adaptador = new Adaptador(this, llenarArrayList(elegirVehiculo));
        listaVehiculos.setAdapter(adaptador);


        listaVehiculos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                v = (Vehiculo) listaVehiculos.getItemAtPosition(position);
                onclick(v);
//                Toast.makeText(getApplicationContext(), "" + v.getNombre(), Toast.LENGTH_LONG).show();
            }
        });
    }


    void onclick(Vehiculo v) {
        Fragment fragmentA = getSupportFragmentManager().findFragmentById(R.id.fragmento_detalle);
        if (fragmentA == null) {
            Intent i = new Intent(this, VehiculoActivity.class);
            i.putExtra("nombre", v.getNombre());
            i.putExtra("ruta", v.getRuta());
            i.putExtra("cv", v.getPotenciaCv());
            i.putExtra("par", v.getParMotor());
            i.putExtra("peso", v.getPeso());
            //Iniciamos la nueva actividad y le pasamos el Intent i  y un int mayor o igual que 0 que indicará al
            //Activity desde el que se lanza que espere una respuesta cuando la otra Actividad sea finalizada
            startActivityForResult(i, -1);
        } else {
            ((FragmentDetalle)getSupportFragmentManager()
                    .findFragmentById(R.id.fragmento_detalle)).mostrarDetalle(v);
        }

    }

    //Instanciando botón de menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //Método se activa al seleccionar cualquier elemento del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.EditText_menu:
                openDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void openDialog() {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.acerca_de);
        dialog.setTitle(R.string.app_name);
        dialog.show();
    }


    private ArrayList<Vehiculo> llenarArrayList(int posSpinner) {
        arrayList = new ArrayList<>();

        InputStream is = getResources().openRawResource((posSpinner == 0) ? R.raw.coches : R.raw.motos);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String linea = "";
        try {
            while ((linea = br.readLine()) != null) {
                String[] array = linea.split(",");
                arrayList.add(new Vehiculo(array[0], array[1], Integer.valueOf(array[2]), Integer.valueOf(array[3]), Integer.valueOf(array[4])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Devolvemos el arrayList con sus datos completados
        return arrayList;
    }
}
