package com.renan.practica9;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Field;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetalle extends Fragment {


    public FragmentDetalle() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detalle, container, false);
    }


    public void mostrarDetalle(Vehiculo v) {

        String ruta = v.getRuta();
        String name = v.getNombre();
        int cv = v.getPotenciaCv();
        int peso = v.getPeso();
        int par = v.getParMotor();
        double cvPorKg = (double) peso / cv;

        TextView tvNombre = getView().findViewById(R.id.textView_nombre_vehiculo);
        TextView tvPotencia = getView().findViewById(R.id.textView_potencia_vehiculo);
        TextView tvPar = getView().findViewById(R.id.textView_par_vehiculo);
        TextView tvPeso = getView().findViewById(R.id.textView_peso_vehiculo);
        TextView tvRelacion = getView().findViewById(R.id.textView_relacion_pesoPontencia_vehiculo);
        tvNombre.setText(name);
        tvPotencia.setText("" + cv + "cv");
        tvPar.setText("" + par + "nm");
        tvPeso.setText("" + peso + "Kg");
        tvRelacion.setText("" + Double.toString(cvPorKg).substring(0, 3) + " Kg/CV");
        ImageView iv = getView().findViewById(R.id.imageView_imagen_vehiculo);


        try {
            Field f = R.drawable.class.getDeclaredField(ruta);
            iv.setImageResource(f.getInt(null));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
                /*
        Poniedo los txtViews visibles
         */
        TextView tvPotenciaTxt = getView().findViewById(R.id.textView_potencia_txt);
        tvPotenciaTxt.setVisibility(View.VISIBLE);

        TextView tvPesoTxt = getView().findViewById(R.id.textView_peso_txt);
        tvPesoTxt.setVisibility(View.VISIBLE);

        TextView tvParTxt = getView().findViewById(R.id.textView_par_txt);
        tvParTxt.setVisibility(View.VISIBLE);

        TextView tvRelacionTxt = getView().findViewById(R.id.textView_relacion_pesoPontencia_vehiculo_txt);
        tvRelacionTxt.setVisibility(View.VISIBLE);
    }
}
